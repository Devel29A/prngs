#include <iostream>

#include "bigint.h"

BigInt::BigInt () {
   mpz_init (i); 
}

BigInt::BigInt (int arg) {
    mpz_init_set_ui (i, arg);
}

BigInt::BigInt (std::string arg, int base) {
    if (mpz_init_set_str (i, arg.c_str (), base) == 0) {
        std::cout << "WARNING: Can't initialize BigInt by " << arg << ". ";
        std::cout << "Zero value is assigned instead." << std::endl;
    }
}

BigInt::BigInt (mpz_t arg) {
    mpz_init_set (i, arg);
}

BigInt::~BigInt () {
    mpz_clear (i);
}

BigInt BigInt::operator= (BigInt arg) {
    mpz_set (i, arg.i);
}

BigInt BigInt::operator= (int arg) {
    mpz_set_ui (i, arg);
}
/*
bool BigInt::operator== (BigInt arg) {
}

bool BigInt::operator!= (BigInt arg) {
}

bool BigInt::operator>= (BigInt arg) {
}

bool BigInt::operator<= (BigInt& arg) {
}

BigInt BigInt::operator+ (BigInt& arg) {
}

BigInt BigInt::operator- (BigInt& arg) {
}

BigInt BigInt::operator/ (BigInt& arg) {
}

BigInt BigInt::operator% (BigInt& arg) {
}

BigInt BigInt::operator* (BigInt& arg) {
}

BigInt BigInt::operator^ (BigInt& arg) {
}

*/

std::string BigInt::toString (int base) {
    std::string s;
    char *buf;
/*    switch (base) {
        case 2:
            break;
        case 8:
            break;
        case 10:
            if (asprintf (&buf, "%Zd\n", i) < 0)
                s = std::string ("");
            else
                s = std::string (buf);
            break;
        case 16:
            break;
        default:
    }
*/    return s;
}

