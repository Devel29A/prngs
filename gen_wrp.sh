#PRNG type
prng=$1

#Generated random number lenght in bits
len=$2

#Number of random keys to be generated
num=$3

#Output file for plot
plot=$prng"_"$len"_"$num".plot.in"

#Output file for fft
fft=$prng"_"$len"_"$num".fft.in"

#Output file for STS tests (dieharder)
sts=$prng"_"$len"_"$num".sts.in"

#Remove previous files if exist
rm -f $plot
rm -f $fft
rm -f $sts

#Prepare header for sts dieharder test file
echo "type: d" >> $sts
echo "count: "$num >> $sts
echo "numbits: "$len >> $sts

#Run generator and store values to output files
while [ $num -gt 0 ] ; do
	rn=`./prng -t $prng -l $len -n 1`
    	#echo "$rn" >> $plot
    	echo "$rn" >> $sts
    	#echo "$i $rn" >> $fft
	let num-=1
done
