#ifndef CRYPTO_UTILS_H
#define CRYPTO_UTILS_H

#include <random>
#include <iostream>
#include "gmp.h"

namespace Crypto {
    class Utils {
        public:
            // Puts n most significant bits of 'in' into 'out'
            static bool getMostSignBits  (const mpz_t in, mpz_t out, unsigned int n);

            // Puts n least significant bits of 'in' into 'out'
            static bool getLeastSignBits (const mpz_t in, mpz_t out, unsigned int n);

            // Generates n-th bit random number ten puts it to 'out'
            static bool getRandomNumber (mpz_t out, unsigned int n);

            // Finds greatest prime divisor
            static bool getGreatestPrimeDivisor (const mpz_t in, mpz_t GPD);
    };

    class PRNG {
        public:
            // Generates n-bit length random number using Blum-Micali PRNG algorithm
            static bool getBlumMicaliSeq (mpz_t out, unsigned int seqLen);

            // Generates n-bit lenght random number using Blum-Blum-Shub PRNG algorithm
            static bool getBlumBlumShubSeq (mpz_t out, unsigned int seqLen);

            // Generates n-bit length random number using RSA PRNG algorithm
            static bool getRSASeq (mpz_t out, unsigned int segLen);

            // Generates n-bith length random number using Linear Congruential Generators 
            static bool getLCGSeq (mpz_t out, unsigned int seqLen);

            // Generates n-bith length random number using Linear Feedback Shift Registers
            static bool getLFSRSeq (mpz_t out, unsigned int seqLen);

            // Generates n-bith length random number using Micali-Schnorr PRNG algorithm
            static bool getMicaliSchnorrSeq (mpz_t out, unsigned int seqLen);
    };
}

#endif
