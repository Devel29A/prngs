#include "crypto-utils.h"

bool Crypto::Utils::getMostSignBits  (const mpz_t in, mpz_t out, unsigned int n) {
    // Get n most significant bits by division 'in' by 2 ^ (bitlen - n)
    // then store result into 'out'
    mpz_tdiv_q_2exp (out, in, mpz_sizeinbase (in, 2) - n);
    return true;
}

bool Crypto::Utils::getLeastSignBits (const mpz_t in, mpz_t out, unsigned int n) {
    // Prepare a bit mask first
    mpz_t mask;
    mpz_init (mask);
    // Make mask looks like 0100...000
    mpz_setbit (mask, n);
    // Make mask looks like 0011...111
    mpz_sub_ui (mask, mask, 1);
    // Get n least significant bits then store result into 'out'
    mpz_and (out, in, mask);
    // Memory free
    mpz_clear (mask);
    return true;
}

bool Crypto::Utils::getRandomNumber (mpz_t out, unsigned int n) {
    // Calculate number of std::random_device random numbers to make n-th bit random number
    // std::random_device returns unsigned int random number
    const unsigned int uIntBitLen = sizeof (unsigned int) * 8;
    unsigned int m = n / uIntBitLen;
    if ((!(n % uIntBitLen)) || (n <= uIntBitLen)) {
        m++;
    }
    std::random_device rd;
    mpz_t rnd;
    mpz_init_set_ui (rnd, rd());
    // Combine m random numbers to get one n-th bit random number 
    for (int i = 0; i < m; i++) {
        // Product of 'out' and 2 ^ uIntBitLen is equal to left shift to uIntBitLen bits
        mpz_mul_2exp (out, out, uIntBitLen);
        // Make bitwise OR to store random number to 'out'
        mpz_ior (out, out, rnd);
        // Get another random number from random_device
        mpz_set_ui (rnd, rd());
    }
    mpz_clear (rnd);
    getLeastSignBits (out, out, n);
    return true;
}

bool Crypto::Utils::getGreatestPrimeDivisor (const mpz_t in, mpz_t GPD) {
    // Try to find the greatest prime divisor of 'in'
    mpz_t g, r;
    mpz_inits (g, r, NULL);
    // Find next prime
    mpz_nextprime (g, g);
    // Definetely the greatest prime devisor of 'in' can't be more than it
    // Continue loop until GPD >= 'in'
    while (mpz_cmp (g, in) < 0) {
        // Calculate reminder
        mpz_tdiv_r (r, in, g);
        if (mpz_cmp_ui (r, 0)) {
            mpz_set (GPD, g);
        }
        mpz_nextprime (g, g);
    }
    mpz_clears (g, r, NULL);
}

bool Crypto::PRNG::getBlumMicaliSeq (mpz_t out, unsigned int seqLen) {
    // This generator gets its security from the difficulty of computing discrete logarithms.
    // Let g be a prime and p be an odd prime. A key x(0), starts off the process:
    //                  x(i+1) = g^x(i) mod p
    // The output of the generator is 1 if x(i) < (p - 1)/2, and 0 otherwise.
    // If p is large enough so that computing discrete logarithms mod p is infeasible, 
    // then this generator is secure. 
    mpz_t b, p, q, g, x_i;
    mpz_inits (b, p, q, g, x_i, NULL);
    
    // Get random n-bit random number and use it as bottom border while search prime
    Utils::getRandomNumber (b, seqLen);

    // Find next prime and put it into p
    mpz_nextprime (p, b);               

    // Calculate (p - 1) / 2
    mpz_sub_ui (q, p, 1);           // q = p - 1
    mpz_tdiv_q_ui (q, q, 2);        // q = q / 2

    // Set g equals to 2
    mpz_init_set_ui (g, 2);
    
    // Set x(0) equals to random seed
    //std::random_device rd;
    //mpz_set_ui (x_i, rd());
    Utils::getRandomNumber (x_i, seqLen);

    for (int i = 0; i < seqLen; i++)
    {   
        // We don't need to calculate x(0)
        if (i > 0)                      
        {
            // Calculate x(i+1) = g^x(i) mod p
            mpz_powm (x_i, g, x_i, p);  
        }
        // If x(i) < (p - 1)/2 then y(i) = 1 otherwise y(i) = 0
        if (mpz_cmp (q , x_i) > 0)      
        {
            mpz_setbit (out, i);
        }
    }

    mpz_clears (b, p, q, g, x_i, NULL);
    return true;
}

bool Crypto::PRNG::getBlumBlumShubSeq (mpz_t out, unsigned int seqLen) {
    // The theory behind the BBS generator has to do with quadratic residues modulo n.
    // First find two large prime numbers, p and q, which are congruent to 3 modulo 4. The product of
    // those numbers, N, is a Blum integer. Choose another random integer, x, which is relatively prime to
    // n. Compute 
    //                              x(0) = x^2 mod N 
    // That's the seed for the generator. Now you can start computing bits. 
    // The i-th pseudo-random bit is the least significant bit of x(ш) , where
    //                              x(i) = x(i-1)^2 mod n
    mpz_t b, p, c, d, q, n, x, GCD, x_0, x_i, b_i;
    mpz_inits (b, p, c, d, q, n, x, GCD, x_0, x_i, b_i, NULL);
    
    // Get random n-bit random number and use it as bottom border while search prime
    Utils::getRandomNumber (b, seqLen);

    // Find next prime and put it into p
    mpz_nextprime (p, b);         

    // Set c equal to 3
    mpz_set_ui (c, 3);             

    // Set d equal to 4
    mpz_set_ui (d, 4);

    // While choosen p prime is not congruent to c modulo d then find next p prime
    while (mpz_congruent_p (p, c, d) == 0)
    {
        mpz_nextprime (p, p);           
    }
    
    // Just to be sure p is not equal to p we need find next q prime
    mpz_nextprime (q, p);               
    
    // While choosen q prime is not congruent to c modulo d then find next q prime
    while (mpz_congruent_p (q, c, d) == 0)
    {
        mpz_nextprime (q, q);           
    }

    // Calculate Blum integer - n
    mpz_mul (n, p, q);
    
    // Set x is equal to random seed
    std::random_device rd;
    mpz_set_ui (x, rd());          

    // Calculate the greatest common divisor of n and x
    mpz_gcd (GCD, n, x);
    
    // Continue the loop until the greatest common divisor is equal to 1
    while (mpz_cmp_ui (GCD, 1) != 0)    
    {
        // If x is not relatively prime to n then pick another random integer
        mpz_set_ui (x, rd());           
        mpz_gcd (GCD, n, x);        
    }

    // Set seed equal to x^2 mod n
    mpz_powm_ui (x_0, x, 2, n);         
    
    // Set x(i) equals to x(0)
    mpz_set (x_i, x_0);

    for (int i = 0; i < seqLen; i++)
    {
        // We don't need to calculate x(0) one more time
        if (i > 0)                          
        {
            // Calculate x(i)
            mpz_powm_ui (x_i, x_i, 2, n);   
        }
        // Just put least sugnificant bit value into b(i) and check
        if (mpz_mod_ui (b_i, x_i, 2) > 0)   
        {
            // Set i-th bit if b(i) is equal to 1
            mpz_setbit (out, i);               
        }
    }
    
    mpz_clears (b, p, c, d, q, n, x, GCD, x_0, x_i, b_i, NULL);
    return true;   
}

bool Crypto::PRNG::getRSASeq (mpz_t out, unsigned int seqLen) {
    // This RSA generator is a modification of Blum-Micali. The initial parameters
    // are a modulus n which is the product of two large primes p and q, 
    // an integer e which is relatively prime to (p - 1) (q - 1),
    //  and a random seed x0, where x0 is less than N.
    mpz_t b, p, q, n, Fp, Fq, F, GCD, x_i, b_i;
    mpz_inits (b, p, q, n, Fp, Fq, F, GCD, x_i, b_i, NULL);
    
    // Get random n-bit random number and use it as bottom border while search prime
    Utils::getRandomNumber (b, seqLen);

    // Find next p prime
    mpz_nextprime (p, b);               

    // Find next q prime
    mpz_nextprime (q, p);

    // Calculates n = p * q
    mpz_mul (n, p, q);                  
    // F = (p - 1) * (q - 1)
    mpz_sub_ui (Fp, p, 1);              
    mpz_sub_ui (Fq, q, 1);              
    mpz_mul (F, Fp, Fq);                

    // If e relative prime to F then GCD must be equal to 1 otherwise 
    // increment e then calculate GCD again
    int e = 2;
    mpz_gcd_ui (GCD, F, e);             
    while (mpz_cmp_ui (GCD, 1) != 0)    
    {
         mpz_gcd_ui (GCD, F, ++e);      
    }

    // Set random seed. It must be less than N otherwise pick another seed value
    std::random_device rd;
    int seed = rd ();                   
    while (mpz_cmp_ui (n, seed) < 0)   
    {
        seed = rd ();                   
    }
    // Set x(0) = seed
    mpz_init_set_ui (x_i, seed);         

    // Generate the sequence
    for (int i = 0; i < seqLen; i++)
    {
        // Don't need to calculate x(0)
        if (i > 0)                      
        {
            // x(i+1) = x(i)^e mod n
            mpz_powm_ui (x_i, x_i, e, n);   
        }
        // Just put least sugnificant bit value into b(i) and check
        if (mpz_mod_ui (b_i, x_i, 2) > 0)   
        {
            // Set i-th bit if 'out' sequence is equal to 1
            mpz_setbit (out, i);               
        }
    }

    mpz_clears (b, p, q, n, Fp, Fq, F, GCD, x_i, b_i, NULL);
    return true; 
}

bool Crypto::PRNG::getLCGSeq (mpz_t out, unsigned int seqLen) {
   	const int a = 45;
	const int c = 21;
	const int m = 1000000;
	
	mpz_t seed, b;
	mpz_inits (seed, b, NULL);

    Utils::getRandomNumber (seed, 16);

	int bitsLen;
	unsigned int x_i = mpz_get_ui(seed);

	for (int i = 0; i < seqLen; i++) {
		if (i > 0) {
			x_i = (a * x_i + c) % m;
			mpz_set_ui (b, x_i);
			bitsLen = mpz_sizeinbase (b, 2);
			mpz_mul_2exp (out, out, bitsLen);
        	mpz_ior (out, out, b);
		}
	}
	
	mpz_clears (seed, b, NULL);

    return true;
}

bool Crypto::PRNG::getLFSRSeq (mpz_t out, unsigned int seqLen) {
	mpz_t seed, b, tmp;
	mpz_inits (seed, b, tmp, NULL);
	
	Utils::getRandomNumber (seed, 32);
	
	unsigned int ShiftRegister = mpz_get_ui (seed);
	
  	int result;
  	int i = 0;
  	
  	int bitsLen;

  	while (i < seqLen) {
  		ShiftRegister = ((((ShiftRegister >> 31)
                         ^ (ShiftRegister >> 6)
                         ^ (ShiftRegister >> 4)
              			 ^ (ShiftRegister >> 2)
      			         ^ (ShiftRegister >> 1)
      			         ^ ShiftRegister)
      			         & 0x1) << 31)
      			         | (ShiftRegister >> 1);
  		result = ShiftRegister & 0x1;
  		mpz_set_ui (b, result);
  		bitsLen = mpz_sizeinbase (b, 2);
		mpz_mul_2exp (out, out, bitsLen);
		mpz_set_ui (tmp, result);
  		mpz_ior (out, out, tmp);
  		i++;
  	}
	
	mpz_clears (seed, b, tmp, NULL);
    
    return true;
}

bool Crypto::PRNG::getMicaliSchnorrSeq (mpz_t out, unsigned int seqLen) {

	mpz_t seed, boardPr, p, q, n, f, ge, gcd, x_i, y_i, z_i, m, most;
	mpz_inits (seed, boardPr, p, q, n, f, ge, gcd, x_i, y_i, z_i, m, most, NULL);
	
	Utils::getRandomNumber (boardPr, 32);

    mpz_nextprime (p, boardPr);
	
    do {
        Utils::getRandomNumber (boardPr, 32);
	    mpz_nextprime(q, boardPr);
    } while (p == q);

    // n = p*q
    mpz_mul (n, p, q);  
	
    mpz_sub_ui (p, p, 1);
    mpz_sub_ui (q, q, 1);
    
    // f = (p-1)*(q-1)
    mpz_mul (f, p, q);  

    int N = mpz_sizeinbase (n, 2);
        
    const int high = 100;
    const int low = 2;
	
	int e = 0;
	
  	do {
        // Increment e then calculate GCD again
	    e = (std::rand()%(high - low + 1)) + low;
        mpz_gcd_ui (gcd, f, e);      
		mpz_mul_ui (ge, ge, 80);
    } while ((mpz_cmp_ui (gcd, 1) != 0) && (mpz_cmp_ui (ge, N) >= 0));
	
	double tte = 1.0 - (2.0/e);
	int k = (int)(N*tte);
	int r = N - k;

	int bitelen = 0;
	
	do {
		Utils::getRandomNumber (seed, r);
		bitelen = mpz_sizeinbase (seed, 2);
	} while (bitelen != r);

    // x(0) = seed
	mpz_set (x_i, seed);			
	
	for (int i = 0; i < seqLen ; i++) {
	      if (i > 0) { 
            // x(i) = (x(i) - 1)^b mod n
			mpz_powm_ui (x_i, x_i, e, n);				
			Utils::getMostSignBits  (x_i, most, r);
			Utils::getLeastSignBits (x_i, z_i, k);
			mpz_mul_2exp (out, out, k);
        	mpz_ior (out, out, z_i);
		}
	}
	
	mpz_clears (seed, boardPr, p, q, n, f, ge, gcd, x_i, y_i, z_i, m, most, NULL);

    return 0;
}

