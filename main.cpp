#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>

#include "gmp.h"
#include "crypto-utils.h"
#include "bigint.h"

using namespace Crypto;

int main (int argc, char ** argv) {
    // First we need to parse passed options
    std::string usage;
    usage += std::string ("usage: \t-t PRNG type <bm|bbs|rsa|lfsr|lcg|ms>\n");
    usage += std::string ("\t-l random sequence length in bits must be greater than zero\n");
    usage += std::string ("\t-n number of sequences to be generated must be greater than zero\n");
    usage += std::string ("\t[-h program usage (this help)]\n");
    int c, seqLen, numOfSeqs;
    std::string seqType;
    extern char *optarg;
    while ((c = getopt(argc, argv, "t:l:n:h")) != -1) {
        switch (c) {
            case 't':
                // Store choosen generator type
                seqType = std::string (optarg);
                break;
            case 'l':
                // Store specified sequence length
                seqLen = std::stoi (optarg);
                break; 
            case 'n':
                // Store specified number of sequences to be generated
                numOfSeqs = std::stoi (optarg);
                
               break;
            case 'h':
                // Just print program usage then exit
                std::cout << usage << std::endl;
                return 0;
                break;
            case '?':
                // In case of unknown options passed print out program usage as well 
                std::cout << usage << std::endl;
                return 0;
                break;
        }
    }

    // Then we have to check input data
    if ((seqType != std::string ("bm"))  && 
        (seqType != std::string ("bbs")) && 
        (seqType != std::string ("rsa")) &&
        (seqType != std::string ("lfcr"))&&
        (seqType != std::string ("lcg")) &&
        (seqType != std::string ("ms"))) {
        // In case of misusing print out program usage then return
        std::cout << usage << std::endl;
        return 0;
    } else if (seqLen <= 0) {
        // In case of misusing print out program usage then return
        std::cout << usage << std::endl;
        return 0;
    } else if (numOfSeqs <= 0) {
        // In case of misusing print out program usage then return
        std::cout << usage << std::endl;
        return 0;
    }

    if (seqType == std::string ("bm")) {
        // Generate random sequence using Blum-Micali PRNG
        mpz_t p;
        mpz_init (p);
        for (int i = 0; i < numOfSeqs; i++) {
            PRNG::getBlumMicaliSeq (p, seqLen);
            mpz_out_str (stdout, 10, p);
            std::cout << std::endl;
        }
        mpz_clear (p);
    } else if (seqType == std::string ("bbs")) {
        // Generate random sequence using Blum-Blum-Shub PRNG
        mpz_t p;
        mpz_init (p);
        for (int i = 0; i < numOfSeqs; i++) {
            PRNG::getBlumBlumShubSeq (p, seqLen);
            mpz_out_str (stdout, 10, p);
            std::cout << std::endl;
        }
        mpz_clear (p);
    } else if (seqType == std::string ("rsa")) {
        // Generate random sequence using RSA PRNG
        mpz_t p;
        mpz_init (p);
        for (int i = 0; i < numOfSeqs; i++) {
            PRNG::getRSASeq (p, seqLen);
            mpz_out_str (stdout, 10, p);
            std::cout << std::endl;
        }
        mpz_clear (p);
    } else if (seqType == std::string ("lcfr")) {
         // Generate random sequence using Linear Congruential Generator
        mpz_t p;
        mpz_init (p);
        for (int i = 0; i < numOfSeqs; i++) {
            PRNG::getLCGSeq (p, seqLen);
            mpz_out_str (stdout, 10, p);
            std::cout << std::endl;
        }
        mpz_clear (p);
    } else if (seqType == std::string ("lcg")) {
        // Generate random sequence using Linear Feedback Shift Registers
        mpz_t p;
        mpz_init (p);
        for (int i = 0; i < numOfSeqs; i++) {
            PRNG::getLFSRSeq (p, seqLen);
            mpz_out_str (stdout, 10, p);
            std::cout << std::endl;
        }
        mpz_clear (p);
    } else if (seqType == std::string ("ms")) {
        // Generate random sequence using Micali-Schnorr PRNG algorithm
        mpz_t p;
        mpz_init (p);
        for (int i = 0; i < numOfSeqs; i++) {
            PRNG::getMicaliSchnorrSeq (p, seqLen);
            mpz_out_str (stdout, 10, p);
            std::cout << std::endl;
        }
        mpz_clear (p);
    }

    return 0;
}
