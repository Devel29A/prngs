all:	prng 

prng:
	g++ main.cpp crypto-utils.cpp bigint.cpp -std=c++11 -g -lgmp -o prng

prng_hub:
	g++ main.cpp crypto-utils.cpp bigint.cpp -I/app/gmp/5.1.2/LMWP3/include/ -L/app/gmp/5.1.2/LMWP3/lib/ -lgmp -std=c++11 -o prng

clean:
	rm -f prng

