#ifndef BIGINT_H
#define BIGINT_H

#include <string>

#include "gmp.h"

class BigInt {
    private:
        mpz_t i;
       
    public:
        BigInt ();
        BigInt (int);
        BigInt (std::string, int);
        BigInt (mpz_t);
        ~BigInt ();
        BigInt operator= (BigInt);
        BigInt operator= (int);
        /*
        bool operator==  (BigInt&);
        bool operator!=  (BigInt&);
        bool operator>=  (BigInt&);
        bool operator<=  (BigInt&);
        BigInt operator+ (BigInt&);
        BigInt operator- (BigInt&);
        BigInt operator/ (BigInt&);
        BigInt operator% (BigInt&);
        BigInt operator* (BigInt&);
        BigInt operator^ (BigInt&);
        */
        std::string toString (int);
};

#endif
